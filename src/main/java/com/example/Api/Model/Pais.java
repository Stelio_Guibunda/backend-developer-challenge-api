package com.example.Api.Model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name ="paises")
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of="id")
public class Pais {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;
    private String capital;
    private Double area;
    private String subregiao;
    private String regiao;

    public Pais(DadosdoPais dados) {
        this.capital= dados.capital();
        this.nome=dados.nome();
        this.regiao=dados.regiao();
        this.subregiao=dados.subregiao();
        this.area= dados.area();
    }
    public void actualizarInformacoes(DadosdoPais dados) {

        if (dados.nome() != null) {

            this.nome = dados.nome();
        }
        if (dados.capital() != null) {
            this.capital= dados.capital();
        }
        if (dados.regiao() != null) {
            this.regiao= dados.regiao();
        }
        if (dados.area() != null) {
            this.area= dados.area();
        }
        if (dados.subregiao() != null) {
            this.subregiao= dados.subregiao();
        }

    }



}
