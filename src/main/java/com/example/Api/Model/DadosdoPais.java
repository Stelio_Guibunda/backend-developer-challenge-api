package com.example.Api.Model;

import jakarta.validation.constraints.NotBlank;

public record DadosdoPais(
        @NotBlank
        String nome,

        Double area,
        String subregiao,
        @NotBlank
        String capital,
        String regiao) {
}
