package com.example.Api.Controller;

import com.example.Api.Model.DadosdoPais;
import com.example.Api.Model.Pais;
import com.example.Api.Repository.PaisRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/paises")
public class PaisController {
    @Autowired
    private PaisRepository repository;

    @PostMapping
    public ResponseEntity<Pais> salvarPais(@RequestBody @Valid DadosdoPais dados){
        var pais= new Pais(dados) ;
        return ResponseEntity.status(HttpStatus.CREATED).body(repository.save(pais));
    }

//    @GetMapping("/{id}")
//    public ResponseEntity<Pais> findById(@PathVariable Long id) {
//        return ResponseEntity.ok(repository.getReferenceById(id));
//    }
    @GetMapping
    public ResponseEntity<List<Pais>> listar(){
        return ResponseEntity.ok(repository.findAll());
    }
    @PutMapping("/{id}")
    public ResponseEntity<Pais> actualizar(@RequestBody @Valid DadosdoPais dados, @PathVariable Long id){

        var pais=repository.getReferenceById(id);
        pais.actualizarInformacoes(dados);
        return ResponseEntity.ok(repository.save(pais));

    }
    @DeleteMapping("/{id}")
    public  ResponseEntity deletar(@PathVariable Long id){
        repository.deleteById(id);
        return ResponseEntity.noContent().build();
    }


    @GetMapping ("/{id}")
    public ResponseEntity<Optional<Pais>> detailCountry(@PathVariable Long id){
        Optional<Pais> obj =repository.findById(id);
        return ResponseEntity.ok().body(obj);
    }


}
