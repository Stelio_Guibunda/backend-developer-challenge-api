package com.example.Api.Controller;

import com.example.Api.Model.Usuario.DataLogin;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")

public class AuthController {
    @Autowired
    private AuthenticationManager manager;
    @PostMapping
    public ResponseEntity loginMethod(@RequestBody @Valid DataLogin Data){
        var token = new UsernamePasswordAuthenticationToken(Data.login(), Data.senha());
        var authentication =manager.authenticate(token);
        return  ResponseEntity.ok().build();

    }


}
