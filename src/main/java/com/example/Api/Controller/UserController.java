package com.example.Api.Controller;

import com.example.Api.Model.Pais;
import com.example.Api.Model.Usuario.User;
import com.example.Api.Model.Usuario.UserRepository;
import com.example.Api.Repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/testando")
public class UserController {
    @Autowired
    private UserRepository repository;
    @GetMapping
    public ResponseEntity<List<User>> listar(){
        return ResponseEntity.ok(repository.findAll());
    }


}
