package com.example.Api.Repository;

import com.example.Api.Model.Pais;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaisRepository extends JpaRepository<Pais,Long> {
}
